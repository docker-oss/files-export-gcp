FROM google/cloud-sdk:alpine
WORKDIR /usr/src/app

RUN apk update
RUN apk --no-cache add jq tzdata tar pigz

COPY compress.sh .

CMD ["sh", "compress.sh"]