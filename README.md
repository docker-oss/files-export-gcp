# files-backup

### Setup

- Copy `docker-compose.yml`
- Copy content of `.env.dist` to `.env`

You should have those 2 files

Then change every variables in `.env`

This should looks like this:

You need to put the key for a service account that can writer inside the bucket.

```
ARCHIVE_PATH="woptimo-wp"

GCP_BUCKET_NAME=bucket-test

GCP_SERVICE_KEY='{
  "type": "service_account",
  "project_id": "conductive-well-323232",
  "private_key_id": "98765REERJHGVFO987654E",
  "private_key": "-----BEGIN PRIVATE KEY-----\n\n-----END PRIVATE KEY-----\n",
  "client_email": "email@conductive-well-323232.iam.gserviceaccount.com",
  "client_id": "98765434564328765",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/email%40conductive-well-323232.iam.gserviceaccount.com"
}'

```