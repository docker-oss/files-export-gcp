ERROR_LOG_LOCATION=/tmp/error.log

#LINE_URL="https://notify-api.line.me/api/notify"
#DISCORD_URL=${DISCORD_URL}

PATH=/path

# ACTIVATE GCLOUD
echo ${GCP_SERVICE_KEY} > /usr/src/app/service.json
if gcloud auth activate-service-account --key-file /usr/src/app/service.json 2>$ERROR_LOG_LOCATION
then
    gcloud config set project $(echo $GCP_SERVICE_KEY | jq .project_key)
    echo "Authenticated to GCP, Backuping..."
   #BACKUP SQL
    if tar cf - $PATH | pigz -9 -p 32 > $ARCHIVE_PREFIX-$(date "+%Y_%m_%d-%H_%M_%S").tar.gz
   then
       echo "Backup finished"
       #UPLOAD GCP
       if gsutil -m cp $ARCHIVE_PREFIX* gs://$GCP_BUCKET_NAME 2>$ERROR_LOG_LOCATION
          then
	      echo "Backup uploaded to GCP"
          else
	      echo "Error GCS Upload"
	      cat $ERROR_LOG_LOCATION
	      message=`cat ${ERROR_LOG_LOCATION}`
	      #curl -X POST -H "Authorization: Bearer ${LINE_TOKEN}" -F "message=${message}" $LINE_URL
              #curl -X POST -H "Content-Type: application/json" -d '{"username":"'"$GCP_BUCKET_NAME"'", "content":"'"${message//\"/\\\"}"'"}' $DISCORD_URL
       fi
       rm $ARCHIVE_PREFIX*
   else
       echo "Error MYSQL Dump"
       cat $ERROR_LOG_LOCATION
       message=`cat ${ERROR_LOG_LOCATION}`
       #curl -X POST -H "Authorization: Bearer ${LINE_TOKEN}" -F "message=${message}" $LINE_URL
       #curl -X POST -H "Content-Type: application/json" -d '{"username":"'"$GCP_BUCKET_NAME"'", "content":"'"${message//\"/\\\"}"'"}' $DISCORD_URL
   fi
else
    echo "Error GCP Auth"
    cat $ERROR_LOG_LOCATION
    message=`cat ${ERROR_LOG_LOCATION}`
    #curl -X POST -H "Authorization: Bearer ${LINE_TOKEN}" -F "message=${message}" $LINE_URL
    #curl -X POST -H "Content-Type: application/json" -d '{"username":"'"$GCP_BUCKET_NAME"'", "content":"'"${message//\"/\\\"}"'"}' $DISCORD_URL
fi
